/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gotham;

import java.io.IOException;

/**
 *
 * @author w10
 */
public class SopaDeLetrasMain {
    public static void main(String[] args) throws IOException{
        Cuadricula sopaDeLetrasConNivel;
        Cuadricula sopaDeLetrasVacia= new Cuadricula(15);
        InsertadorDePalabras nuevoInsertador = new InsertadorDePalabras();
        sopaDeLetrasConNivel = nuevoInsertador.insertarNivelFacil(sopaDeLetrasVacia, "normal");
        sopaDeLetrasConNivel.imprimirCuadricula();
    }
}
